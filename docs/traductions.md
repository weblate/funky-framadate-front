# Traductions

Les chaînes sont traduites via des clés de fichiers JSON
exemple: [src/assets/i18n/FR.json]()

La mise en place de weblate pour faire des traductions collaboratives reste à réaliser
https://framagit.org/framasoft/framadate/funky-framadate-front/-/issues/112

Nous avons récupéré les chaînes de traduction du projet Framadate v1, il faudra faire un nettoyage des clés et valeurs inutilisées dans les fichiers de traduction.
