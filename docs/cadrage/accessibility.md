# Accessibilité
Vérifiez toujours que ce que vous développez est Accessible pour des personnes aux handicaps divers tels que le préconise le WECAG.
https://www.w3.org/Translations/WCAG20-fr/

Quelques lignes à vérifier:
* Couleurs contrastées
* ordre des liens cohérent
* textes des liens éloquents
* textes alternatifs aux images les décrivant
* possibilité de changer la taille des textes et le thème visuel
* couleurs lisibles pour les personnes daltoniennes
* raccourcis claviers. alt+t pour passer a un autre thème visuel par exemple.
* attributs aria pour les lecteurs d'écran
* performance de chargement pour les petits débits
* server side rendering
* pas d'éléments perdus en dehors de l'écran, faites du mobile first
* parcimonie: si un élément n'est pas prévu pour être montré sur petit écran, ne le mettez pas sur grand écran sous prétexte qu'il y a plus de place.
* less is more
* système de recherche basique

N'ayez pas peur de demander de l'aide
