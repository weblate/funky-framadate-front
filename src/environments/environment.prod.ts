const productionBaseUrl = 'https://framadate-api.cipherbliss.com'; // set this to your production domain
const apiVersion = 'v1';

const backendApiUrlsInDev = {
	local: `/api/${apiVersion}`,
	remote: `${productionBaseUrl}/api/${apiVersion}`,
};
const apiV1 = {
	baseHref: `${productionBaseUrl}/api/${apiVersion}`,
	api_new_poll: '/poll/',
	api_get_poll: '/poll/{id}',
	api_new_vote_stack: '/vote-stack',
	'api_test-mail-poll': `/api/${apiVersion}/poll/mail/test-mail-poll/{emailChoice}`,
	'app.swagger': '/api/doc.json',
};

export const environment = {
	advanced_options_display: true,
	appLogo: 'assets/img/logo.png',
	appTitle: 'FramaDate',
	appVersion: '2.2.1',
	autofill_creation: false,
	autofill_default_timeslices: false,
	autofill_participation: false,
	autoSendNewPoll: false,
	creation_display_admin_url: false,
	creation_display_hour_per_day: false, // display or not the ability to choose time slices different per day
	creation_display_proposals_time_slices: false,
	creation_email_is_required: true,
	display_header_create_button: false,
	display_menu_creation: false,
	display_routes: false,
	enable_colored_weekend_days: false,
	expiresDaysDelay: 30,
	frontDomain: productionBaseUrl,
	interval_days_default: 7,
	maxCountOfAnswers: 300,
	production: true,
	showDemoWarning: false,
	showStepperShortcuts: false,

	api: {
		versionToUse: 'apiV1',
		version: {
			apiV1,
		},
		baseHref: backendApiUrlsInDev.remote,
		endpoints: {
			polls: {
				name: '/poll',
				choices: {
					name: '/choices',
				},
				comments: {
					name: '/comments',
				},
				slugs: {
					name: '/slugs',
				},
				answers: {
					name: '/answers',
				},
			},
			users: {
				name: '/users',
				polls: {
					name: '/polls',
					sendEmail: {
						name: '/send-email',
					},
				},
			},
		},
	},
	poll: {
		defaultConfig: {
			maxCountOfAnswers: 150,
			expiresDaysDelay: 60,
			expiracyAfterLastModificationInDays: 180,
			whoCanChangeAnswers: 'everybody',
			visibility: 'link_only',
			voteChoices: 'only_yes',
		},
	},
	localStorage: {
		key: 'FramaSondage',
	},
	display_validation_errors: false,
};
