// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { apiV1, endpoints } from './endpoints';
import { poll_conf } from './poll_conf';
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';

endpoints.baseHref = apiV1.baseHref;

export const environment = {
	advanced_options_display: true,
	appLogo: 'assets/img/logo.png',
	appTitle: 'FramaDate',
	appVersion: '2.2.1',
	autofill_creation: false,
	autofill_default_timeslices: false,
	autofill_participation: false,
	autoSendNewPoll: false,
	creation_display_admin_url: false,
	creation_display_hour_per_day: false, // display or not the ability to choose time slices different per day
	creation_display_proposals_time_slices: false,
	creation_email_is_required: true,
	display_header_create_button: false,
	display_menu_creation: false,
	display_routes: false, // demo paths to test polls
	enable_colored_weekend_days: false, // color differently the weekend days
	expiresDaysDelay: 30,
	frontDomain: 'http://127.0.0.1:4200',
	interval_days_default: 7,
	maxCountOfAnswers: 300,
	production: false,
	showDemoWarning: false,
	showStepperShortcuts: false,

	api: endpoints,
	poll: poll_conf,
	localStorage: {
		key: 'FramaDateFunky',
	},
	display_validation_errors: false,
};

// Included with Angular CLI.
