import { Component, Input, OnInit } from '@angular/core';
import { Poll } from '../../../../core/models/poll.model';
import { FormGroup } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { PollService } from '../../../../core/services/poll.service';

@Component({
	selector: 'app-advanced-config',
	templateUrl: './advanced-config.component.html',
	styleUrls: ['./advanced-config.component.scss'],
})
export class AdvancedConfigComponent implements OnInit {
	public urlPrefix = '/participation/';
	public environment = environment;
	public displayClearPassword = false;
	@Input()
	public poll?: Poll;
	@Input()
	public form: FormGroup;
	constructor(public pollService: PollService) {}

	ngOnInit(): void {}
}
