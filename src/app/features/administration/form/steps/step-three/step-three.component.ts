import { Component, Input, OnInit } from '@angular/core';
import { PollService } from '../../../../../core/services/poll.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { environment } from '../../../../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-step-three',
	templateUrl: './step-three.component.html',
	styleUrls: ['./step-three.component.scss'],
})
export class StepThreeComponent implements OnInit {
	@Input()
	step_max: any;
	@Input()
	form: any;
	public environment = environment;

	constructor(public pollService: PollService, private titleService: Title) {
		this.pollService.step_current = 3;
		this.step_max = this.pollService.step_max;
		this.titleService.setTitle(
			' Création de sondage - Étape ' +
				this.pollService.step_current +
				' sur ' +
				this.pollService.step_max +
				' ' +
				environment.appTitle
		);
	}

	ngOnInit(): void {}

	drop(event: CdkDragDrop<string[]>) {
		// moveItemInArray(this.pollService.choices, event.previousIndex, event.currentIndex);
	}
	convertDateInputs() {
		this.pollService.mode_calendar
			? this.pollService.convertCalendarToText()
			: this.pollService.convertTextToCalendar();
	}

	changeDateInputMode() {
		this.convertDateInputs();
		this.pollService.mode_calendar = !this.pollService.mode_calendar;
	}
}
