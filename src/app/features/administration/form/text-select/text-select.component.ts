import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'app-text-select',
	templateUrl: './text-select.component.html',
	styleUrls: ['./text-select.component.scss'],
})
export class TextSelectComponent implements OnInit {
	@Input()
	public form: FormGroup;
	public choices = [];

	constructor() {}

	ngOnInit(): void {}
	reinitChoices(): void {}
	addChoice(): void {}
	deleteChoiceField(i): void {}
	keyOnChoice($event, i): void {}
}
