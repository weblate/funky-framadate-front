import { Component, Input, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { Poll } from '../../../core/models/poll.model';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { DateUtilitiesService } from '../../../core/services/date.utilities.service';

@Component({
	selector: 'app-success',
	templateUrl: './success.component.html',
	styleUrls: ['./success.component.scss'],
})
export class SuccessComponent {
	@Input() poll: Poll;
	mailToRecieve: string;
	window: any = window;
	environment = environment;
	today: Date = new Date();
	constructor(public pollService: PollService, private dateUtils: DateUtilitiesService, private titleService: Title) {
		this.titleService.setTitle(
			environment.appTitle + ' - 🎉 succès de création de sondage - ' + this.pollService.form.value.title
		);

		this.pollService.poll.subscribe((newpoll: Poll) => {
			this.poll = newpoll;
		});
	}

	sendToEmail() {
		alert('todo');
	}

	getExpiracyDateFromPoll(poll: Poll): Date {
		return this.dateUtils.addDaysToDate(poll.default_expiracy_days_from_now, new Date());
	}
}
