import { ChangeDetectorRef, Component, Inject, Input } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
	selector: 'app-stepper',
	templateUrl: './stepper.component.html',
	styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent {
	@Input()
	public stepperConfirm: HTMLElement;

	@Input()
	public step_current: number = 1;
	@Input()
	public step_max: number = 5;
	public environment = environment;
	display_cancel_dialog: boolean = false;

	constructor(
		public pollService: PollService,
		@Inject(DOCUMENT) private document: any,
		private cd: ChangeDetectorRef,
		private router: Router
	) {
		this.step_current = this.pollService.step_current;
	}

	showCancelDialog() {
		this.display_cancel_dialog = true;
		this.cd.detectChanges();
		let buttonClosepopup = this.document.querySelector('#close_dialog');
		if (buttonClosepopup) {
			buttonClosepopup.focus();
			console.log('button close found');
		} else {
			console.log('not found');
		}
	}
	focusOnCancelButton() {
		this.display_cancel_dialog = false;
		let buttonClose = this.document.querySelector('#display_cancel_popup_button');
		if (buttonClose) {
			buttonClose.focus();
		}
	}

	goToHome() {
		this.display_cancel_dialog = false;
		this.router.navigate(['/']);
	}
}
