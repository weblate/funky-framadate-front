import { Component, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';

@Component({
	selector: 'app-consultation-user',
	templateUrl: './consultation-user.component.html',
	styleUrls: ['./consultation-user.component.scss'],
})
export class ConsultationUserComponent implements OnInit {
	constructor(public pollService: PollService) {}

	ngOnInit(): void {}
}
