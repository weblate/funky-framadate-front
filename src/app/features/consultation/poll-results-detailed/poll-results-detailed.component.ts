import { Component, Input } from '@angular/core';

import { Poll } from '../../../core/models/poll.model';
import { PollService } from '../../../core/services/poll.service';
import { StorageService } from '../../../core/services/storage.service';
import { ToastService } from '../../../core/services/toast.service';
import { ApiService } from '../../../core/services/api.service';

@Component({
	selector: 'app-poll-results-detailed',
	templateUrl: './poll-results-detailed.component.html',
	styleUrls: ['./poll-results-detailed.component.scss'],
})
export class PollResultsDetailedComponent {
	@Input() public poll: Poll;

	constructor(
		private pollService: PollService,
		public storageService: StorageService,
		private api: ApiService,
		private toastService: ToastService
	) {}

	stackHasVotesForChoice(stack, choice: any) {
		return undefined !== stack.votes[choice];
	}

	getValue(stack, choice: any) {
		if (this.stackHasVotesForChoice(stack, choice)) {
			return stack.votes[choice].value;
		}
		return null;
	}

	/**
	 * create a	new vote stack
	 */
	addVoteStack(): void {
		this.storageService.vote_stack.poll_custom_url = this.poll.custom_url;
		this.toastService.display('envoi du vote ....');
		this.api
			.sendNewVoteStackOfPoll(this.storageService.vote_stack)
			.then((resp: any) => {
				console.log('sendNewVoteStackOfPoll resp', resp);
				this.toastService.display('bien enregistré');
				// reload stack
			})
			// eslint-disable-next-line @typescript-eslint/unbound-method
			.catch(this.api.ousideHandleError);
	}

	/**
	 * update existing vote stack
	 * @param Stack
	 */
	updateVoteStack(): void {
		const vote_stack = this.storageService.vote_stack;
		vote_stack.poll_custom_url = this.poll.custom_url;

		console.log('updateVoteStack vote_stack.votes', vote_stack.votes.length, vote_stack.votes);
		const handlingError = this.api.ousideHandleError;

		this.api
			.sendUpdateVoteStack(vote_stack)
			.then((resp) => {
				console.log('sendUpdateVoteStack updated resp', resp);
				this.storeVoteStackAndReloadPoll(resp);
				this.toastService.display('vote bien mis à jour', 'success');
			})
			.catch(handlingError);
	}
	/**
	 * store the updated vote stack
	 * @param voteStack
	 */
	storeVoteStackAndReloadPoll(voteStack: any) {
		if (voteStack.status == 200) {
			this.storageService.mapVotes(voteStack.data);
			this.pollService.enrichVoteStackWithCurrentPollChoicesDefaultVotes(this.storageService.vote_stack);
			if (this.pollService.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(this.poll.custom_url, this.pollService.pass_hash);
			} else {
				this.pollService.loadPollByCustomUrl(this.poll.custom_url);
			}
		} else {
			this.toastService.display('erreur à l enregistrement');
		}
	}

	make_date(name: string) {
		name = name.substr(0, 30);
		return new Date(name);
	}

	make_display_range_time(name: string) {
		return name.replace('>>>', 'de ');
	}
}
