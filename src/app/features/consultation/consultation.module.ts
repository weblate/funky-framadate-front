import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { ConsultationRoutingModule } from './consultation-routing.module';
import { ConsultationComponent } from './consultation.component';
import { PollResultsCompactComponent } from './poll-results-compact/poll-results-compact.component';
import { PollResultsDetailedComponent } from './poll-results-detailed/poll-results-detailed.component';
import { ChoiceButtonComponent } from '../../shared/components/choice-item/choice-button.component';
import { PasswordPromptComponent } from './password/password-prompt/password-prompt.component';
import { ChoiceDetailsComponent } from '../../shared/components/choice-details/choice-details.component';
import { CoreModule } from '../../core/core.module';
import { ConsultationLandingComponent } from './consultation-landing/consultation-landing.component';
import { ConsultationUserComponent } from './consultation-user/consultation-user.component';
import { SuccessComponent } from './success/success.component';
import { AdministrationModule } from '../administration/administration.module';

@NgModule({
	declarations: [
		ConsultationComponent,
		PollResultsCompactComponent,
		PollResultsDetailedComponent,
		ChoiceButtonComponent,
		PasswordPromptComponent,
		ConsultationLandingComponent,
		ConsultationUserComponent,
		SuccessComponent,
	],
	imports: [
		CommonModule,
		ConsultationRoutingModule,
		SharedModule,
		TranslateModule.forChild({ extend: true }),
		AdministrationModule,
	],
})
export class ConsultationModule {}
