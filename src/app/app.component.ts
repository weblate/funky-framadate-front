import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { environment } from '../environments/environment';
import { Theme } from './core/enums/theme.enum';
import { LanguageService } from './core/services/language.service';
import { ThemeService } from './core/services/theme.service';
import { NavigationEnd, Route, Router, RouterOutlet } from '@angular/router';
import { slideInAnimation } from './shared/animations/main';
import { FramaKeyboardShortcuts } from './shared/shortcuts/main';
import { ShortcutEventOutput, ShortcutInput } from 'ng-keyboard-shortcuts';
import { PollService } from './core/services/poll.service';
import { Poll } from './core/models/poll.model';
import { PollDTO } from './core/models/poll.DTO.model';
import { PrimeNGConfig } from 'primeng/api';
import { Language } from './core/enums/language.enum';
import { ApiService } from './core/services/api.service';
import { DOCUMENT } from '@angular/common';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [slideInAnimation],
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
	shortcuts: ShortcutInput[] = FramaKeyboardShortcuts;

	public appTitle: string = environment.appTitle;
	public appLogo: string = environment.appLogo;
	public themeClass: string;
	public isSidebarOpened = false;
	public devModeEnabled = !environment.production;
	public environment = environment;
	public onHomePage = false;
	private themeSubscription: Subscription;

	constructor(
		private router: Router,
		private titleService: Title,
		private themeService: ThemeService,
		private pollService: PollService,
		private apiService: ApiService,
		private config: PrimeNGConfig,
		@Inject(DOCUMENT) private document: any,
		private languageService: LanguageService // private mockingService: MockingService
	) {}

	printpath(parent: string, config: Route[]) {
		for (let i = 0; i < config.length; i++) {
			const route = config[i];
			if (route.children) {
				const currentPath = route.path ? parent + '/' + route.path : parent;
				this.printpath(currentPath, route.children);
			}
		}
	}

	ngOnInit(): void {
		this.languageService.getPrimeNgStrings().subscribe((resp) => {
			this.config.setTranslation(resp);
		});

		this.printpath('', this.router.config);
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			console.log('evt', evt);

			this.onHomePage = evt.url === '/';
			setTimeout(() => {
				window.scrollTo(0, 0);
			}, 10);
		});

		if (!environment.production) {
			this.appTitle += ' [DEV]';
		}

		let loadedPoll;
		if (this.pollService.poll) {
			loadedPoll = this.pollService.poll;
		}

		this.titleService.setTitle(this.appTitle + ' - ' + loadedPoll.title);
		this.languageService.configureAndInitTranslations();
		this.themeSubscription = this.themeService.theme.subscribe((theme: Theme) => {
			switch (theme) {
				case Theme.DARK:
					this.themeClass = 'theme-dark-crystal';
					break;
				case Theme.CONTRAST:
					this.themeClass = 'theme-hot-covid';
					break;
				case Theme.RED:
					this.themeClass = 'theme-hot-covid';
					break;
				default:
					this.themeClass = 'theme-light-watermelon';
			}
		});

		// debug cors
		// this.apiService.getAllAvailablePolls();
	}

	ngAfterViewInit(): void {
		this.shortcuts.push(
			{
				key: '?',
				label: 'Help',
				description: 'Question mark',
				command: (e) => console.log('question mark clicked', { e }),
				preventDefault: true,
			},
			{
				key: ['up up down down left right left right b a enter'],
				label: 'Sequences',
				description: 'Konami code!',
				command: (output: ShortcutEventOutput) => console.log('Konami code!!!', output),
			},
			{
				key: ['cmd + b'],
				label: 'Help',
				description: 'Cmd + b',
				command: (e) => console.log(e),
				preventDefault: true,
			}
		);
	}

	ngOnDestroy(): void {
		if (this.themeSubscription) {
			this.themeSubscription.unsubscribe();
		}
	}

	prepareRoute(outlet: any) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
	}
}
