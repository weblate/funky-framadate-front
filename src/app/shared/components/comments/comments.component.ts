import { Component, Input } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import * as moment from 'moment';
import { Stack } from '../../../core/models/stack.model';
import { Poll } from '../../../core/models/poll.model';

@Component({
	selector: 'app-comments',
	templateUrl: './comments.component.html',
	styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent {
	@Input() public vote_stack: Stack;
	@Input() public poll: Poll;

	public config: any = {
		myName: '',
		myEmail: '',
		myComment: '',
	};

	constructor(private pollService: PollService) {}

	calculateDaysAgoOfComment(dateAsString) {
		let numberOfDays = 0;

		if (dateAsString && dateAsString) {
			numberOfDays = moment(new Date()).diff(moment(new Date(dateAsString)), 'days');
		}

		return numberOfDays;
	}

	addComment() {
		alert('TODO');
	}
}
