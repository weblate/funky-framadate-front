import { Component, OnInit } from '@angular/core';

import { Language } from '../../../../core/enums/language.enum';
import { LanguageService } from '../../../../core/services/language.service';
import { StorageService } from '../../../../core/services/storage.service';

@Component({
	selector: 'app-language-selector',
	templateUrl: './language-selector.component.html',
	styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent implements OnInit {
	public currentLang: Language;
	public availableLanguages: any = [];
	language: string;

	constructor(private languageService: LanguageService, private storageService: StorageService) {}

	ngOnInit(): void {
		this.availableLanguages = this.languageService.getAvailableLanguages();

		this.currentLang = this.languageService.getLangage();
	}

	setLang(): void {
		this.languageService.setLanguage(this.currentLang);
	}

	nextLang(): void {
		const index = this.availableLanguages.indexOf(this.currentLang);
		console.log('this.currentLang ', this.currentLang, 'index', index);

		if (index !== -1) {
			// on passe au language suivant si il existe, sinon on revient au numéro 0
			const nextlang = console.log('this.availableLanguages[index + 1]', this.availableLanguages[index + 1]);
			console.log('nextlang', nextlang);

			if (this.availableLanguages[index + 1]) {
				console.log('lang suivante', this.availableLanguages[index + 1]);
				this.currentLang = this.availableLanguages[index + 1];
			} else {
				console.log('retour à la langue 0');
				this.currentLang = this.availableLanguages[0];
			}
			this.setLang();
		}
		console.log('this.currentLang ', this.currentLang);

		console.log('this.availableLanguages', this.availableLanguages);
		console.log('TODO');
	}
}
