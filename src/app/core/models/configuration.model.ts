import { environment } from '../../../environments/environment';
import { DateService } from '../services/date.service';

export class PollConfiguration {
	constructor(
		public allowComments: boolean = true,
		public areResultsPublic: boolean = true,
		public dateCreated: Date = new Date(Date.now()),
		public password: string = '',
		public isAboutDate: boolean = false,
		public isAllowingtoChangeOwnAnswers: boolean = true,
		public isMaybeAnswerAvailable: boolean = false,
		public isProtectedByPassword: boolean = false,
		public isOwnerNotifiedByEmailOnNewVote: boolean = false,
		public isOwnerNotifiedByEmailOnNewComment: boolean = false,
		public isZeroKnoledge: boolean = true,
		public hasSeveralHours: boolean = false,
		public hasMaxCountOfAnswers: boolean = false,
		public whoCanChangeAnswers: string = environment.poll.defaultConfig.whoCanChangeAnswers, // everybody, self, nobody (= just admin)
		public visibility: string = environment.poll.defaultConfig.visibility, // visible to anyone with the link:
		public voteChoices: string = environment.poll.defaultConfig.voteChoices, // possible answers to a vote timeSlice: only "yes", "yes, maybe, no": number = environment.poll.defaultConfig.maxCountOfAnswers,
		public maxCountOfAnswers: number = environment.poll.defaultConfig.maxCountOfAnswers,
		public expiresDaysDelay: number = environment.poll.defaultConfig.expiresDaysDelay,
		public expiracyAfterLastModificationInDays: number = environment.poll.defaultConfig
			.expiracyAfterLastModificationInDays,
		// date after creation day when people will not be able to vote anymore
		public expiracyDate: Date = DateService.addDaysToDate(
			environment.poll.defaultConfig.expiresDaysDelay,
			new Date(Date.now())
		)
	) {}

	public static isArchived(configuration: PollConfiguration): boolean {
		return configuration.expiracyDate ? DateService.isDateInPast(configuration.expiracyDate) : undefined;
	}
}
