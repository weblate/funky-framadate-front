import { Component, Inject } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { StorageService } from '../../services/storage.service';
import { ApiService } from '../../services/api.service';
import { ToastService } from '../../services/toast.service';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
	public environment = environment;
	public nonexistent_email = '';
	public email_sent = false;
	constructor(
		@Inject(DOCUMENT) private document: any,
		public storageService: StorageService,
		public toastService: ToastService,
		public titleService: Title,
		private api: ApiService
	) {
		this.titleService.setTitle(environment.appTitle + ' - Accueil ');
	}

	searchMyPolls() {
		const email = this.storageService.vote_stack.owner.email;
		this.email_sent = false;
		this.api.findMyPollsByEmail(email).then(
			(resp) => {
				console.log('resp', resp);

				if (resp) {
					if (resp.data && resp.data.mail_sent == '1') {
						this.toastService.display("C'est bon, vérifiez votre boite mail");
						this.email_sent = true;
					}
				}
			},
			(error) => {
				if (error.response.status == '404') {
					this.toastService.display('Aucun sondage géré par cet email : ' + email);
					this.nonexistent_email = email;
					this.document.querySelector('#search_email').select();
				}
				console.log('error', error);
			}
		);
	}
}
