import { Injectable } from '@angular/core';
import { defaultTimeOfDay } from '../../../../mocks/old-stuff/config/defaultConfigs';
import { DateChoice } from '../models/dateChoice.model';
import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root',
})
export class DateUtilitiesService {
	/**
	 * add some dateChoices to a date, to compute intervals
	 * @param days
	 * @param date
	 */
	addDaysToDate(days: number, date: Date) {
		date = new Date(date.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}

	/**
	 *
	 * @param d1
	 * @param d2
	 * @param interval
	 */
	getDatesInRange(d1: Date, d2: Date, interval: number) {
		d1 = new Date(d1);
		d2 = new Date(d2);
		const dates = [];
		while (+d1 < +d2) {
			dates.push({
				literal: this.formateDateToInputStringNg(d1),
				date_object: d1,
			});
			d1.setDate(d1.getDate() + interval);
		}
		return [...dates];
	}

	/**
	 * get the number of dateChoices between two dates
	 * @param d1
	 * @param d2
	 */
	dayDiff(d1: Date, d2: Date): any {
		const div = 1000 * 3600 * 24;
		return Math.abs((d2.getTime() - d1.getTime()) / div).toFixed(0);
	}

	/**
	 * format a date object to the date format used by the inputs of type date
	 * YYYY-MM-DD
	 * @param date
	 */
	formateDateToInputStringNg(date: Date): string {
		return [
			date.getFullYear(),
			this.getDoubleDigits(date.getMonth() + 1),
			this.getDoubleDigits(date.getDate()),
		].join('-');
	}

	/**
	 * transform a date object to a short date string
	 * @param inputDate
	 */
	parseInputDateToDateObject(inputDate: Date): Date {
		const boom = inputDate.toISOString().substring(0, 10).split('-');

		const converted = new Date(boom['0'], boom['1'] - 1, boom['2']);
		console.log('converted', converted);
		return converted;
	}

	/**
	 * get double in string
	 * @param str
	 */
	getDoubleDigits(str) {
		return ('00' + str).slice(-2);
	}

	/**
	 * compute the number of dateChoices in the date interval
	 * @param startDateInterval
	 * @param endDateInterval
	 */
	countDays(startDateInterval: Date, endDateInterval: Date): number {
		if (endDateInterval && startDateInterval) {
			return this.dayDiff(endDateInterval, startDateInterval);
		}
		return 0;
	}

	makeDefaultCalendarDateChoices(): Date[] {
		return [
			this.addDaysToDate(1, new Date()),
			this.addDaysToDate(2, new Date()),
			this.addDaysToDate(3, new Date()),
		];
	}
	/**
	 * fill default dates for today + the next 3 dateChoices
	 */
	makeDefaultDateChoices(): DateChoice[] {
		const today = new Date();
		const ladate2 = this.addDaysToDate(1, today);
		const ladate3 = this.addDaysToDate(2, today);
		const ladate4 = this.addDaysToDate(3, today);

		return [
			this.convertDateToDateChoiceObject(ladate2),
			this.convertDateToDateChoiceObject(ladate3),
			this.convertDateToDateChoiceObject(ladate4),
		];
	}

	/**
	 * convert a date to a DateChoice
	 * @param date
	 */
	convertDateToDateChoiceObject(date: Date): DateChoice {
		let isUnder10 = date.getDate() < 10;
		let day = isUnder10 ? `0${date.getDate()}` : date.getDate();
		// get month is based on 0, so yeah
		let input = `${date.getFullYear()}-${date.getMonth() + 1}-${day}`;
		return {
			literal: this.formateDateToInputStringNg(date),
			timeSlices: environment.autofill_default_timeslices ? Object.create(defaultTimeOfDay) : [],
			date_object: date,
			date_input: input,
		};
	}
}
