# Framadate - funky version
![landing_page](docs/img/landing_page.png)
FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API backend.

EN: A libre polling software made by contributors around the French association Framasoft.
This version uses a brand new backend API.

 - l'API de backend est en PHP / Symfony, ses sources disponible sur framagit https://framagit.org/tykayn/date-poll-api

## Pour débuter - getting started
[lire la doc pour débuter votre Funky Framadate](docs/GETTING_STARTED.md)

## Design et maquettes
Maquette naviguable:
https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c/a/rbYngRP/play

Planche de création de sondage, https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c/p/9F41E940-BAAF-489A-8C90-AC6E5C762211

Planche des participations https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c/p/BFC83003-AAF3-4858-9EE1-F28A88D11305


## Documentation
FR: Toute la documentation est disponible [dans le dossier "doc"](/docs), principalement en Français.
EN: All documentation is available in the "doc" folder, mainly in French because reasons.


# Version funky framadate

* maquettes par Thomas Bonamy : 
	* icones https://feathericons.com/
	
* [Spécifications](docs/cadrage/specifications-fonctionnelles)
* maquettes par @maiwann : https://scene.zeplin.io/project/5d4d83d68866d6522ff2ff10
* vidéo de démo des maquettes par @maiwann : https://nuage.maiwann.net/s/JRRHTR9D2akMAa7
* discussions sur framateam canal général : https://framateam.org/ux-framatrucs/channaels/framadate
* discussions techniques côté développeurs : https://framateam.org/ux-framatrucs/channels/framadate-dev
* [notes de réunion](docs/reunions)
* [traductions](docs/traductions)


## Screenshots - exemples de maquette de la nouvelle version
![funky_framadate_maquette](docs/img/Screenshot_2021-04-19%20migration%20depuis%20un%20Framadate%20version%201.jpeg)
![funky_framadate_maquette](docs/img/advanced_config.png)
![funky_framadate_maquette](docs/img/preview_creation.png)
![funky_framadate_maquette](docs/img/kind_poll.png)
![funky_framadate_maquette](docs/img/colors_framadate_funky.png)
![funky_framadate_maquette](docs/img/framdate_funky_design.png)

# Documentations sur Angular

* `{- sur sass -}` (on va utiliser CSS, si angular permet d'avoir des variables CSS, @newick)


## LIBRARIES USED

|     status      | lib choice_label                                                       | usage                                                     |
| :-------------: | -------------------------------------------------------------- | --------------------------------------------------------- |
|                 | [axios](https://github.com/axios/axios)                        | http client                                               |
|                 | [bulma](https://bulma.io/)                                     | CSS framework                                             |
|                 | [chart.js](https://www.chartjs.org/)                           | PrimeNG solution for graphs. (Chart.js installs MomentJS) |
|                 | [compodoc](https://compodoc.app/)                              | Generate technic documentation                            |
|                 | ESlint, Prettier, Lint-staged                                  | Format & lint code                                        |
|                 | [fork-awesome](https://forkaweso.me)                           | Icons collection                                          |
|                 | [fullcalendar](https://fullcalendar.io/docs/initialize-es6)    | PrimeNG solution to manage & display calendars            |
|                 | [husky](https://www.npmjs.com/package/husky)                   | Hook actions on commit                                    |
|                 | [jest](https://jestjs.io/)                                     | test engine                                               |
|                 | [json-server](https://www.npmjs.com/package/json-server)       | local server for mocking data backend                     |
|     removed     | [locale-enum](https://www.npmjs.com/package/locale-enum)       | enum of all locales                                       |
|                 | [momentJS](https://momentjs.com/)                              | manipulate dates. (chartJS’s dependency)                  |
| to be installed | [ng2-charts](https://valor-software.com/ng2-charts/)           | Manipulate graphs along with chart.js                     |
|                 | [ngx-clipboard](https://www.npmjs.com/package/ngx-clipboard)   | Handle clipboard                                          |
|                 | [ngx-markdown](https://www.npmjs.com/package/ngx-markdown)     | markdown parser                                           |
|                 | [ngx-webstorage](https://www.npmjs.com/package/ngx-webstorage) | handle localStorage & webStorage                          |
|                 | [primeNG](https://www.primefaces.org/primeng/)                 | UI components collection                                  |
|                 | [quill](https://www.npmjs.com/package/quill)                   | powerful rich text editor. WYSIWYG.                       |
| to be installed | [short-uuid](https://www.npmjs.com/package/short-uuid)         | generate uuid                                             |
|     removed     | [storybook](https://storybook.js.org/)                         | StyleGuide UI                                             |
|                 | [ts-mockito](https://www.npmjs.com/package/ts-mockito)         | Mocks for testing.                                        |
|  to be removed  | [uuid](https://www.npmjs.com/package/uuid)                     | generate uuid                                             |

---
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.
